/*
SQLyog Community v13.2.0 (64 bit)
MySQL - 10.4.25-MariaDB : Database - db_kel
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_kel` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_kel`;

/*Table structure for table `arabica` */

DROP TABLE IF EXISTS `arabica`;

CREATE TABLE `arabica` (
  `rbcId` char(12) NOT NULL,
  `rbcNama` varchar(255) DEFAULT NULL,
  `rbcAsal` text DEFAULT NULL,
  `rbcChar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rbcId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `arabica` */

insert  into `arabica`(`rbcId`,`rbcNama`,`rbcAsal`,`rbcChar`) values 
('1','Aceh Gayo Arabika','Aceh Tengah','Strong body and aroma'),
('2','Toraja Arabika','Sulawesi Selatan','Has a distinctive earthy scent'),
('3','Wamena Arabika','Papua','Has a smooth and balanced flavor'),
('4','Mandheling Arabika','Sumatera Utara','Distinctive flavors of spices that blend together');

/*Table structure for table `robusta` */

DROP TABLE IF EXISTS `robusta`;

CREATE TABLE `robusta` (
  `rbstId` char(12) NOT NULL,
  `rbstNama` varchar(255) DEFAULT NULL,
  `rbstAsal` text DEFAULT NULL,
  `rbstChar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rbstId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `robusta` */

insert  into `robusta`(`rbstId`,`rbstNama`,`rbstAsal`,`rbstChar`) values 
('1','Lampung Robusta','Lampung','Strong body'),
('2','Flores Robusta','NTT','Low acidity'),
('3','Pagar Alam Robusta','Lampung','Hints of earth and tobacco'),
('4','Java Mocha Robusta','Jawa','Lightly scented with spices'),
('5','Kopi Tanjung Senang','Tanjung Senang','Happy');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(1,'admin','tes','28b662d883b6d76fd96e4ddc5e9ba780','Admin'),
(2,'admin2','Admin2','7a8a80e50f6ff558f552079cefe2715d','Admin'),
(4,'tess1','tes11','22daf1a39b6e5ea16554f59e472d96f6','Admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
